let inputOfTask = document.getElementById("taskInput");
let submitTaskBtn = document.getElementById("submitTaskInput");
let toDoWrapp = document.getElementById('toDoWrapp');
let spanTaskList = document.getElementsByClassName('text-task');
const TASK_LIST_KEY = 'task-list'

let taskList = [
	{
		taskName: '',
		isCompleted: false
	},
	{
		taskName: '',
		isCompleted: false
	}
];

let inititalToDoWrapp = function () {
	return `
    <div class="to-do-item">
		<span class="text-task"></span>
		<input type="checkbox" class="check-btn-list" onclick="completeTask(0)">
		<button class="delete" onclick="deleteTask(0)">
			<i class="icon-trash">  
			</i>
		</button>
	</div>
	<div class="to-do-item" id="task-1">
		<span class="text-task"></span>
		<input type="checkbox" class="check-btn-list" onclick="completeTask(1)">
		<button class="delete" onclick="deleteTask(1)">
			<i class="icon-trash">  
			</i>
		</button>
	</div>
    `
}

let createHtmlToDoItem = function (lenghtOfCheckList) {
	let firstIndex = lenghtOfCheckList++;
	return `
        <div class="to-do-item">
			<span class="text-task"></span>
			<input type="checkbox"  class="check-btn-list" onclick="completeTask(${firstIndex})">
			<button class="delete" onclick="deleteTask(${firstIndex})">
				<i class="icon-trash">  
				</i>
			</button>
		</div>
    `
};

let apdateTaskList = function () {
	if (localStorage.taskList) {
		taskList = (JSON.parse(localStorage.getItem('taskList')));
	}
};

let chanchCompleteStyle = function (index) {
	if (taskList[index].completed) {
		spanTaskList[index].classList.add('completed');
	} else {
		spanTaskList[index].classList.remove('completed')
	};
}

let changeAttributteChbox = function (index) {
	let checkList = document.querySelectorAll('.check-btn-list')
	if (taskList[index].completed) {
		checkList[index].setAttribute('checked', '');
	} else {
		checkList[index].removeAttribute('checked', '');
	};
}

let fillHtmlToDoWrapp = function () {
	toDoWrapp.innerHTML = inititalToDoWrapp();
	taskList.forEach(function (item, index) {
		if (spanTaskList.length >= taskList.length) {
			spanTaskList[index].innerHTML = taskList[index].description;
		} else {
			let lenghtOfCheckList = document.querySelectorAll('.check-btn-list').length;
			toDoWrapp.innerHTML += createHtmlToDoItem(lenghtOfCheckList);
			spanTaskList[index].innerHTML = taskList[index].description;
		};
		chanchCompleteStyle(index);
		changeAttributteChbox(index);
	})
};

let completeTask = function (index) {
	taskList[index].completed = !taskList[index].completed;
	chanchCompleteStyle(index); //change
	localStorage.setItem('taskList', JSON.stringify(taskList));
}

let deleteTask = function (index) {
	taskList.splice(index, 1);
	localStorage.setItem('taskList', JSON.stringify(taskList));
	apdateTaskList();
	fillHtmlToDoWrapp();
}

apdateTaskList(); // update
fillHtmlToDoWrapp();

submitTaskBtn.addEventListener('click', function () {
	loadXMLDoc()
	if (inputOfTask.value === '') {
		alert('Please, writte a "To-do Item"');
	} else {
		let task = {
			description: inputOfTask.value,
			completed: false
		};
		taskList.push(task);
		inputOfTask.value = '';
		localStorage.setItem('taskList', JSON.stringify(taskList));
		fillHtmlToDoWrapp();
	}
});



function loadXMLDoc() {
	var xmlhttp = new XMLHttpRequest();

	xmlhttp.onreadystatechange = function () {
		if (xmlhttp.readyState == XMLHttpRequest.DONE) {   // XMLHttpRequest.DONE == 4
			if (xmlhttp.status == 200) {
				// document.getElementById("myDiv").innerHTML = xmlhttp.responseText;
				let response = JSON.parse(xmlhttp.responseText)
				let cont = document.getElementsByClassName('container')[0]
				cont.style.backgroundImage = 'url(' + response[0].url + ')'
			}
			else if (xmlhttp.status == 400) {
				console.log('There was an error 400');
			}
			else {
				console.log('something else other than 200 was returned');
			}
		}
	};

	xmlhttp.open("GET", "https://api.thecatapi.com/v1/images/search?limit=5", true);
	xmlhttp.send();
}

loadXMLDoc()